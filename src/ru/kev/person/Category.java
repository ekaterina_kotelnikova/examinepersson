package ru.kev.person;

/**
 * Перечисление категорий для преподавателей
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public enum Category {
    без_категории,
    первая,
    высшая
}
