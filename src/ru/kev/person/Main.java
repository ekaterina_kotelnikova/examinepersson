package ru.kev.person;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс для нахождения:
 * из списка студентов информации о всех девушках поступивших в 2014 году;
 * из списка преподавателей сведений о кураторах.
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Main {
    static Scanner scanner = new Scanner(System.in);
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        System.out.println("Введите количество студентов: ");
        int countOfStudents = scanner.nextInt();

        System.out.println("Введите количество преподавателей: ");
        int countOfTeachers = scanner.nextInt();

        ArrayList<Student> students = new ArrayList<>();
        ArrayList<Teacher> teachers = new ArrayList<>();

        fillingAnArrayStudents(countOfStudents, students);
        fillingAnArrayTeachers(countOfTeachers, teachers);

        theOutputArrayStudents(students);
        theOutputArrayTeachers(teachers);

        findWomanWhoStartOf2014(students);
        findCurators(teachers);


    }

    /**
     * Этот метод позволяет вывести сведения о кураторах.
     *
     * @param teachers ArrayList объектов Teacher
     */

    public static void findCurators(ArrayList<Teacher> teachers) {
        System.out.println("Ищем сведения о кураторах...");
        for (int i = 0; i < teachers.size(); i++) {
            if (teachers.get(i).getCuratorOrNot()) {
                System.out.println(teachers.get(i));
            }
        }
    }

    /**
     * Этот метод позволяет вывести информацию о девушках, поступивших в 2014 году
     *
     * @param students ArrayList объектов Student
     */

    public static void findWomanWhoStartOf2014(ArrayList<Student> students) {
        System.out.println("Ищем девушек, поступивших в 2014 году...");
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getYearOfStart() == 2014 & students.get(i).getGender().equals("Жен")) {
                System.out.println(students.get(i));
            }
        }
    }

    /**
     * Этот метод позволяет вывести элементы из ArrayList объектов Teacher
     *
     * @param teachers ArrayList объектов Teacher
     */

    public static void theOutputArrayTeachers(ArrayList<Teacher> teachers) {
        for (int i = 0; i < teachers.size(); i++) {
            System.out.println(teachers.get(i));
        }
    }

    /**
     * Этот метод позволяет вывести элементы из ArrayList объектов Student
     *
     * @param students ArrayList объектов Student
     */

    public static void theOutputArrayStudents(ArrayList<Student> students) {
        for (int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i));
        }
    }

    /**
     * Метод для ввода с клавиатуры информации о преподавателях
     *
     * @param countOfTeachers количество преподавателей
     * @param teachers        ArrayList объектов Teacher
     * @throws IOException исключения при ошибке чтения из файла
     */

    public static void fillingAnArrayTeachers(int countOfTeachers, ArrayList<Teacher> teachers) throws IOException {
        for (int i = 0; i < countOfTeachers; i++) {
            teachers.add(new Teacher());
            teachers.get(teachers.size() - 1).input();
        }
    }

    /**
     * Метод для ввода с клавиатуры информации о студентах
     *
     * @param countOfStudents количество студентов
     * @param students        ArrayList объектов Student
     * @throws IOException исключения при ошибке чтения из файла
     */

    public static void fillingAnArrayStudents(int countOfStudents, ArrayList<Student> students) throws IOException {
        for (int i = 0; i < countOfStudents; i++) {
            students.add(new Student());
            students.get(students.size() - 1).input();
        }
    }
}
