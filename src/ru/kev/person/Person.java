package ru.kev.person;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Этот класс позволяет описать человека с характеристиками:
 * Фамилия;
 * Пол;
 * Год рождения.
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Person {
    static Scanner scanner = new Scanner(System.in);
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private String surname;
    private String gender;
    private int yearOfBirth;

    public Person(String surname, String gender, int yearOfBirth) {
        this.surname = surname;
        this.gender = gender;
        this.yearOfBirth = yearOfBirth;
    }

    /**
     * Конструктор по умолчанию со значениями:
     * Фамилия - нет значения, пол - ?, год рождения - 0.
     */
    public Person(){
        this("нет значения","?" , 0 );
    }

    /**
     * Метод позволяет ввести данные: Фамилия, Пол, Год рождения
     * с клавиатуры.
     * @throws IOException исключения при ошибке чтения из файла
     */
    public void input() throws IOException {
        System.out.println("Введите фамилию: ");
        surname = reader.readLine();
        System.out.println("Введите пол(Жен/Муж): ");
        gender = reader.readLine();
        System.out.println("Введите год рождения: ");
        yearOfBirth = scanner.nextInt();
    }

    public String getSurname() {
        return surname;
    }

    public String getGender() {
        return gender;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    @Override
    public String toString() {
        return surname +
                ", пол " + gender +
                ", год рождения " + yearOfBirth;
    }
}
