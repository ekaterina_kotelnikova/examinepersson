package ru.kev.person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Этот класс, помимо характеристик класса Person, имеет дополнительные характеристики студента:
 * год поступления;
 * код специальности.
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Student extends Person {
    static Scanner scanner = new Scanner(System.in);
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private int yearOfStart;
    private String specialtyCode;

    public Student(String surname, String gender, int yearOfBirth, int yearOfStart, String specialtyCode) {
        super(surname, gender, yearOfBirth);
        this.yearOfStart = yearOfStart;
        this.specialtyCode = specialtyCode;
    }

    public Student() {
        this("нет значения", "?", 0, 0, "");
    }

    /**
     * Метод позволяет ввести данные: Год поступления, Код специальности
     * с клавиатуры.
     *
     * @throws IOException исключения при ошибке чтения из файла
     */
    public void input() throws IOException {
        System.out.println("Студент ");
        super.input();
        System.out.println("Введите год поступления: ");
        yearOfStart = scanner.nextInt();
        System.out.println("Введите код специальности: ");
        specialtyCode = reader.readLine();

    }

    public int getYearOfStart() {
        return yearOfStart;
    }

    public String getSpecialtyCode() {
        return specialtyCode;
    }

    public void setYearOfStart(int yearOfStart) {
        this.yearOfStart = yearOfStart;
    }

    public void setSpecialtyCode(String specialtyCode) {
        this.specialtyCode = specialtyCode;
    }

    @Override
    public String toString() {
        return "Студент " + getSurname() +
                ", пол " + getGender() +
                ", год рождения " + getYearOfBirth() +
                ", год поступления " + yearOfStart +
                ", код специальности " + specialtyCode;
    }
}
