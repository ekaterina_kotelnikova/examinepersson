package ru.kev.person;

import java.io.IOException;

/**
 * Этот класс, помимо характеристик класса Person, имеет дополнительные характеристики преподавателя:
 * является ли куратором или нет;
 * категория.
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Teacher extends Person {
    private boolean curatorOrNot;
    private Category category;

    public Teacher(String surname, String gender, int yearOfBirth, boolean curatorOrNot, Category category) {
        super(surname, gender, yearOfBirth);
        this.curatorOrNot = curatorOrNot;
        this.category = category;
    }

    public Teacher() {
        this("нет значения", "?", 0, false, Category.без_категории);
    }

    /**
     * Этот метод позволяет ввести данные: является куратором или нет, категорию
     * с клавиатуры
     *
     * @throws IOException исключения при ошибке чтения из файла
     */
    public void input() throws IOException {
        System.out.println("Преподаватель");
        super.input();
        System.out.println("Введите Да, если преподаватель является куратором, иначе введите Нет ");
        String curator = reader.readLine();
        if (curator.equals("Да")) {
            curatorOrNot = true;
        } else {
            curatorOrNot = false;
        }

        System.out.println("Введите 0, если у преподавателя нет категории; " +
                "1 - если первая категория; " +
                "2 - если высшая категория ");
        String enteredCategory = reader.readLine();

        if (enteredCategory.equals("0")) {
            category = Category.без_категории;
        }
        if (enteredCategory.equals("1")) {
            category = Category.первая;
        }
        if (enteredCategory.equals("2")) {
            category = Category.высшая;
        }

    }

    public boolean getCuratorOrNot() {
        return curatorOrNot;
    }

    public Category getCategory() {
        return category;
    }

    public void setCuratorOrNot(boolean curatorOrNot) {
        this.curatorOrNot = curatorOrNot;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Преподаватель " + getSurname() +
                ", пол " + getGender() +
                ", год рождения " + getYearOfBirth() +
                ", " + (curatorOrNot? "является" : "не является")
                + " куратором, категория: " + category;
    }
}
